package algos.patternmatch.fuzzy;

import algos.patternmatch.Ricardo2DPatternMatchGeneric;
import mytools.common.utils.math.coordinates.BoxCoords;

import java.awt.image.BufferedImage;
import java.util.List;

public class FuzzyColorCompareImageMatcher {
    private Ricardo2DPatternMatchGeneric<ImageFuzzyMatchable> matcher = new Ricardo2DPatternMatchGeneric<>();

    public List<BoxCoords> search(BufferedImage image, BufferedImage subImage){
        return matcher.search(new CloseColorsMatcher(image,3), new CloseColorsMatcher(subImage,3));
    }
}
