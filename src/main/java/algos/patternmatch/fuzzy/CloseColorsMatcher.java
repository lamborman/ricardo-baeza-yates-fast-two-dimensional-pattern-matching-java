package algos.patternmatch.fuzzy;

import algos.patternmatch.Matchable;
import algos.patternmatch.fuzzy.ImageFuzzyMatchable;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static mytools.common.utils.file.ResourceLoader.loadResource;

public class CloseColorsMatcher implements Matchable<ImageFuzzyMatchable> {
    private final BufferedImage image;

    private int fuzzyDistance;

    public CloseColorsMatcher(BufferedImage image, int fuzzyDistance) {
        this.image = image;
        this.fuzzyDistance = fuzzyDistance;
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public ImageFuzzyMatchable getValue(int x, int y) {
        int rgb = image.getRGB(x, y);
        Color c = new Color(rgb);
        return new ImageFuzzyMatchable(c.getRed(), c.getGreen(), c.getBlue(), fuzzyDistance);
    }
//305 249
    public static void main(String[] args) throws IOException {
        BufferedImage im2 = ImageIO.read(new File("C:\\mystuff\\dev\\ec\\java\\tdd-sharky\\src\\test\\resources\\images\\mock\\gui\\clients\\_888\\gameTables\\3chipPot.png"));
        BufferedImage im = ImageIO.read(loadResource("images/gui/clients/_888/gametable/chips/chip1c.png"));
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 3; j++) {
                int [] ch = new int [3];
                im.getRaster().getPixel(i,j, ch);
                System.out.println(ch[0] + ", " + ch[1] + ", " + ch[2]);
                im2.getRaster().getPixel(305 +i, 249 + j, ch);
                System.out.println(ch[0] + ", " + ch[1] + ", " + ch[2]);
                System.out.println();
            }
        }
    }

}
