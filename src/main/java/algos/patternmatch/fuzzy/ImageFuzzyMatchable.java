package algos.patternmatch.fuzzy;

public class ImageFuzzyMatchable {
    private int red;
    private int green;
    private int blue;

    private int valueFuzziness;

    public  ImageFuzzyMatchable(int red, int green, int blue, int valueFuzziness) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.valueFuzziness = valueFuzziness;
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() {
        return blue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ImageFuzzyMatchable that = (ImageFuzzyMatchable) o;

        if (Math.abs(red - that.red) > valueFuzziness) return false;
        if (Math.abs(green - that.green) > valueFuzziness) return false;
        return Math.abs(blue - that.blue) <= valueFuzziness;
    }

    @Override
    public int hashCode() {
        return 1;
    }
}
