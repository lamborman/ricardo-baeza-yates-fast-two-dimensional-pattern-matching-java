package algos.patternmatch;

import mytools.common.utils.math.coordinates.BoxCoords;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.util.List;

import static mytools.common.utils.file.ImageLoader.loadImage;
import static org.junit.Assert.assertEquals;

public class MatchScenariosTest {
    Ricardo2DPatternMatchBufferedImage matcher = new Ricardo2DPatternMatchBufferedImage();

    @Test
    public void testEdgesDifferentScenario(){
        BufferedImage image = loadImage("imageWithRepeatedPattern.png");
        BufferedImage subImage = loadImage("pattern.png");
        List<BoxCoords> result = matcher.search(image, subImage);
        assertEquals(1, result.size());
    }
}
