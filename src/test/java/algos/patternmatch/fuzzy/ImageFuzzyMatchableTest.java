package algos.patternmatch.fuzzy;

import org.junit.Test;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ImageFuzzyMatchableTest {

    @Test
    public void testEquality(){
        assertEquals(new ImageFuzzyMatchable(24,9,55,3),new ImageFuzzyMatchable(25,9,55,3));
        assertEquals(new ImageFuzzyMatchable(24,8,55,3),new ImageFuzzyMatchable(24,9,56,3));
        assertEquals(new ImageFuzzyMatchable(24,9,55,3),new ImageFuzzyMatchable(23,11,54,3));
        assertNotEquals(new ImageFuzzyMatchable(20,9,55,3),new ImageFuzzyMatchable(24,9,55,3));
    }

    @Test
    public void testSet(){
        Set<ImageFuzzyMatchable> bag = new HashSet<>();
        bag.add(new ImageFuzzyMatchable(24,9,55,3));
        bag.add(new ImageFuzzyMatchable(25,10,55,3));
        bag.add(new ImageFuzzyMatchable(29,10,55,3));
        assertEquals(2, bag.size());
    }

}